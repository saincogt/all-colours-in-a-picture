# Sancho's Side Notes 

Colour `rgb(r, g, b)` has `r, g, b` three arguments. Each of the argument can be a value between `[0 - 255]`. As the given requirement, we have a step of 8, which means the value of each argument can be a value such as `[0, 8, 16, ... 248]`. In total we can have `(256 / 8) ^ 3 = 32,768` different colours.

> need to note that the `255` will be the maximum value for those 3 arguments, there can not be any `256`. `rgb(255, 255, 255) ` should be the colour with the biggest argument values.

After trying nested `for` loops to generate all the colours, my MacBook was crashed right away because the application was taking up too much memory, then I realised this way won't work. After done some research, I found there is indeed an elegant way of doing it -- bitwise operations.

As each argument of the `rgb` has 32 possible values, we will only need 5 bits in binary to represent them:
> [00000, 00001 ... 11111] can represent 0 to 31, with the total of 32 numbers.

In another words, we will need 15 bits for all the 3 `rgb` arguments. The good thing of the bitwise operations is, each of the 5 bits is "independent", for example:

> Given any 15 bits binary number 11111XXXXXXXXXX, we can tell that value `r` would be 31, as in this scenario the last 10 bits are "irrelevant", whatever values are they won't affect the first 5 bits.

Using this method, we can avoid nested `for` loops, and it becomes possible for us to run everything in only one `for` loop instead.



---
### [Updates on 13 May 2020:]

Had tried the whole day rendering every colour as a tiny component, crashed my browser for dozens of times. Of course, it happened, I was trying to render 32,768 separate components on a page, that is definitely a HUGE number.

Then I realised it would be the wrong way to do it. A better way of doing it may be drawing all the colours on a canvas and render the canvas on the page. Let's see if that works.



---
### [Updates on 14 May 2020:]

1. About getting all the `rgb` values, there is an efficient way -- as each of the argument `(r, g, b)` can be represented in 5 bits binary numbers, we can use bitwise operators such as `&, |, <<, >>` to get each of the arguments. Lets say we have an input `x` with value  of `32,768` -- in binary it would be `11111 11111 11111`, then:
```javascript
// shift x 10 bits to the right, it will get the first 5 bits of the input number
// AND 0x1f (in binary 11111), to prevent if the input number is greater than expected, only keep the 5 bits 
r = (x >> 10) & 0x1f;

// shift x 5 bits to the right, it will get the first 10 bits of the input number
// then AND with 0x1f (in binary 11111), it will return the last 5 bits and disgard the first 5 bits
g = (x >> 5) & 0x1f;

// whatever the input is, AND with 0x1f (in binary 11111), only keep the last 5 bits
b = x & 0x1f;
```

Of course, each of the variables now only can represent value from 0 to 31, if we want a value from 0 to 256, we can easily convert them by multiplying 8 (or left shift 3 bits in binary: `<< 3`, which I personaly think is a better way):

```javascript
r = ((x >> 10) & 0x1f) << 3;
g = ((x >> 5) & 0x1f) << 3;
b = (x & 0x1f) << 3;
```

Done! Now we got all the colours! :)

2. Updates at 22:22pm on the same day:

It seems to be no point separating `r, g, b` 3 arguments, we actually can have a single 15 bits integer to represent all of them. We can get their values by using the bitwise operators when we need them. In another words, more efficient way of getting all the colours is to store an array of numbers from `[0, 1, ...32767]`, instead of storing an array of array `[[0,0,0], [0,0,8] ...[248, 248, 248]]`. We can extract neccesary parts of the number when we actually going to use them. For example:

> when we going to use `r` value, we can simply run `(number & 0x7c00) >> 10` to extract the first 5 bits of `number`.

1. Updates at 22:53pm:

Another thought: Maybe we don't even need to store a binary number for the colour, we are going to draw each of the pixels one by one anyway, we can use the counter when we start the iteration loop. I will leave that for tomorrow. Let's see if that works. :)

---
### [Updates on 15 May 2020:]

There will be some computing in our application, the performance should be considered carefully. I think a better way of doing the calculation is to use bitwise operation more often. For example, if we square number a lot, we should better handle it by bitwise operations, I find it quite useful and very likely to be used in this project:

```javascript
// NOTE: `^` is square in following equations
// If `n` is even, it can be written as
n = 2 * x 
n ^ 2 = (2 * x) ^ 2 = 4 * x ^ 2

//If n is odd, it can be written as 
n = 2 * x + 1
n ^ 2 = (2 * x + 1) ^ 2 = 4 * x ^ 2 + 4 * x + 1
```

That means, we can use a iterated function & bitwise operations to save time and memory.

---
### [Updates on 17 May 2020:]

RGB colours are actually in 3 dimensions. We need to sort them before drawing on to a canvas. There are a few popular methods, I was convienced by this article: http://alienryderflex.com/hsp.html

The key value of the HSP is called `Perceived brightness`, which is:

```javascript
const PD = sqrt(0.299 * R * R + 0.587 * G * G + 0.114 * B * B);
```

By sorting a RGB colour's PD, we can sort the 3 dimensional colours into 1 dimension. However, we need to note that the original RGB value should be kept to draw the image (in our case the value will be the same as the index [0 - 32,767]).

---
### [Updates on 18 May 2020:]

I decided to draw the Mandelbrot Set. The basic formula is:

```javascript
z(n+1) = z(n) ^ 2 + c
```

Note: It starts from `z(0)=0`. `c` is a complex number ( `c` can be represented as coordinates `x,y` in quadratic map).

If sequence `z(n)` is trending to infinite, then it means `c` is not in the set. 

We can by iterating through the canvas to find out which pair of `x,y` are in the set, and then we can by the total count of the pairs split all the colours into 2 groups.

Then we can shuffle the 2 sets of colours, and render the Mandelbrot Set.
