import React, { Fragment, useRef, useEffect } from 'react';
import Header from './header';
import { getSortedColours, shuffle } from './functions';
import { isInMandelbrot, getSetLength, setColour } from './mandelbrot';
import './App.css';

const App = () => {
	const canvasRef = useRef();
	const initialiseCanvas = () => {
		const { width: WIDTH, height: HEIGHT } = canvasRef.current;
		const context = canvasRef.current.getContext('2d');
		const sortedColours = getSortedColours(WIDTH, HEIGHT);
		let i = getSetLength(WIDTH, HEIGHT);
		let coloursNotInSet = shuffle(sortedColours.splice(i));
		let coloursInSet = shuffle(sortedColours.splice(0, i));

		let j = 0;
		let k = 0;
		for (let x = 0; x < WIDTH; x++) {
			for (let y = 0; y < HEIGHT; y++) {
				let isInSet = isInMandelbrot(x, y);
				if (isInSet) {
					context.fillStyle = setColour(coloursInSet, j);
					context.fillRect(x, y, 1, 1);
					j++;
				} else {
					context.fillStyle = setColour(coloursNotInSet, k);
					context.fillRect(x, y, 1, 1);
					k++;
				}
			}
		}
	};

	useEffect(() => initialiseCanvas());

	return (
		<Fragment>
			<Header />
			<div className='content-container'>
				<h2>Mandelbrot Set Image with All 15 bits Colours</h2>
				<p>
					The following Mandelbrot Set image contains all 15 bits rgb colours (32,768 colours, 256px * 128px). Refresh to redraw the image.
				</p>
			</div>
			<div className='picture-container'>
				<canvas id='canvas' width='256' height='128' ref={canvasRef} className='canvas' />
			</div>
		</Fragment>
	);
};

export default App;
