import React from 'react';
import { AppBar, Toolbar } from '@material-ui/core';
import Logo from './assets/logo.svg';
import './header.css';

const Header = () => (
	<AppBar position='sticky' className='app-bar'>
		<img src={Logo} alt='React To-do List' className='logo' />
		<Toolbar>allRGB Code Challenge - Sancho's Implementation</Toolbar>
	</AppBar>
);

export default Header;
