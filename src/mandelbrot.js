// Feel free to adjust those factors to generate different displays
const ZOOM_FACTOR = 128;
const X_FACTOR = 2;
const Y_FACTOR = 1.3;

/**
 * return boolean result if a (x, y) coordinates in the Mandelbrot set
 * @param {number} x coordinate x in the image
 * @param {number} y coordinate y in the image
 */
export const isInMandelbrot = (x, y) => {
	let X = x / ZOOM_FACTOR - X_FACTOR;
	let Y = y / ZOOM_FACTOR - Y_FACTOR;

	let realPart = x / ZOOM_FACTOR - X_FACTOR;
	let imaginePart = y / ZOOM_FACTOR - Y_FACTOR;
	for (let i = 0; i < 10; i++) {
		let tmpRealPart = realPart ** 2 - imaginePart ** 2 + X;
		let tmpImaginePart = 2 * realPart * imaginePart + Y;
		realPart = tmpRealPart;
		imaginePart = tmpImaginePart;
	}
	if (realPart * imaginePart < 5) return true;
	return false;
};

/**
 * return the lenght of the Mandelbrot Set
 * @param {number} width 
 * @param {number} height 
 */
export const getSetLength = (width, height) => {
	let i = 0;
	for (let x = 0; x < width; x++) {
		for (let y = 0; y < height; y++) {
			let belongsToSet = isInMandelbrot(x, y);
			if (belongsToSet) i++;
		}
	}
	return i;
};

/**
 * return a string representing a rgb colour
 * @param {array} array 
 * @param {number} index 
 */
export const setColour = (array, index) => {
	return `rgb(${(array[index][0] & 0x7c00)>>7}, ${(array[index][0] & 0x3e0)>>2}, ${(array[index][0] & 0x1f)<<3})`;
}