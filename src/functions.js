/**
 * input a number and return its square
 * @param {number} n 
 */
const square = (n) => {
	if (n === 0) return 0;
	if (n < 0) n = -n;
	let x = n >> 1;
	if (n & 1) return (square(x) << 2) + (x << 2) + 1;
	return square(x) << 2;
};

/**
 * return hsp 'perceived brightness' value
 * @param {number} number a number between [0, 32767] which can represents a rgb colour
 */
const rgb2hsp = (number) => {
	let r = (number & 0x7c00) >> 7;
	let g = (number & 0x3e0) >> 2;
	let b = (number & 0x1f) << 3;
	return Math.sqrt(0.299 * square(r) + 0.587 * square(g) + 0.114 * square(b));
};

/**
 * return a sorted 2-dimensional array by their hsp values
 * 1st column is original index, which can represent its rgb value
 * 2nd column is hsp value
 * @param {number} width 
 * @param {number} height 
 */
export const getSortedColours = (width, height) => {
	let tmpArray = [];
	for (let index = 0; index < width * height; index++) {
		tmpArray = [...tmpArray, [index, rgb2hsp(index)]];
	}
	return tmpArray.sort((a, b) => a[1] - b[1]);
};

/**
 * return a randomly shuffled array
 * @param {array} array 
 */
export const shuffle = (array) => {
	let currentIndex = array.length;
	while (0 !== currentIndex) {
		let randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;
		let tmp = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = tmp;
	}
	return array;
};
