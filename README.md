# allRGB - Code Challenge

## Code Challenge Requirement Details

You need to write a program which will produce an image in which each colour occurs exactly once -- with no repetition and no used colours. Colours are formed by combining a red, green, and blue component in the range 0..256; your program will need to break each component into 32 steps -- 8, 16,24, .. 256 -- which means you will have 32,768 discrete colours. Your image will therefore need to be sized to accommodate exactly this number of unique colours -- for example, 256x128px (although other dimensions are possible).

The result should be aesthetically pleasing (or at least interesting), and should not use any existing graphics or other files. Your submission will be evaluated in terms of good development practices followed, cleanliness of code, the elegance of the algorithm you use, and the originality of your approach.

You may write this program preferably in Reactjs & Nodejs for use on a web browser which displays its results on-screen.

## FAQ

1. Are third-party open source libraries such as React, Angular, etc. allowed in my submission? 
> Yes, but please include any dependencies (or instructions for installation) with your submission.

2. What browsers need to be supported? 
> If a web-based submission, your submission should work in all modern standards-compliant browsers.

3. What version of Java/JavaScript/Python can I use? 
> Any is fine

4. What about 0 values?
> You can safely ignore the 0 values for each colour component; they're not necessary in this task.

5. Does the image have to be 256x128px in size? 
> No -- you can use any dimensions you like, so long as the image contains each colour exactly once.

6. Can I use third party css/js plotting library?
> No

## Available Scripts

In the project directory, you can run:

### `yarn`

Install the dependencies to run the application.

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
